// const source = 'Level'
// const source = 'Levels'
const source = 'Yo banana boy'

console.log(isPalindrome(source));

function isPalindrome(str) {
  str = str.toLowerCase()
    .replace(/\s+/g, '')
  return str === str.split('').reverse().join('');
}