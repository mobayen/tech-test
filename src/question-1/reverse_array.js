
const arr = ['a1', 'a2', 'a3', 'a4']
const reversed = reverse(arr)

// Log to console
console.log(reversed)

function reverse(arr) {
  let temp = []

  for(i=arr.length; i--; i>0) {
    temp.push(arr[i])
  }

  return temp;
}